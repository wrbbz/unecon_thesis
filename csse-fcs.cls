%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% LaTeX class for dissertations
%
% Designed for SPbSPU, FCS, CSSE department
%
% Created by: Marat Akhin (akhin@kspt.ftk.spbstu.ru)
% Based on: `disser` package
% Distrubuted under: LPPL 1.3+
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{csse-fcs}[2013/01/20 CSSE FCS dissertation class]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{kvoptions}
\RequirePackage{geometry}
\RequirePackage{nomencl}
\RequirePackage{indentfirst}
\RequirePackage{xstring}
\RequirePackage{float}
\RequirePackage{fancyhdr}
\RequirePackage{setspace}

\AtEndOfClass{\RequirePackage{caption}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\SetupKeyvalOptions{
  family=csse,
  prefix=csse@
}
\DeclareStringOption{titlefile}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newif\if@afourpaper\@afourpapertrue
\DeclareOption{a4paper}{
  \@afourpapertrue
}
\if@afourpaper
  \PassOptionsToClass{a4paper,14pt}{disser}
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newif\if@usedotsinheaders\@usedotsinheadersfalse
\DeclareOption{dotsinheaders}{
  \@usedotsinheaderstrue
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newif\if@mastertitle\@mastertitletrue
\DeclareOption{master}{
  \@mastertitletrue
}
\ifx\csse@titlefile\@empty
  \if@mastertitle \renewcommand*{\csse@titlefile}{csse-master.rtx} \fi
\fi
\AtEndOfClass{\input{\csse@titlefile}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\PassOptionsToClass{
  oneside,
  titlepage,
  openright,
  onecolumn
}{disser}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{disser}}

\ProcessKeyvalOptions{kspt}
\ProcessOptions\relax

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\LoadClass{disser}[2011/11/29]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\if@afourpaper
  \geometry{
    paper=a4paper,
    nohead=true,
    includefoot=true,
    left=30mm,
    right=10mm,
    top=20mm,
    bottom=20mm,
    footskip=15mm
  }
  \setlength{\fieldhshift}{8.5cm}
  \renewcommand{\nomlabelwidth}{3cm}
  \onehalfspacing
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand\blankpagecontent[1]{
  \gdef\@blankpagecontent{#1}
}
\blankpagecontent{}
\newcommand\makeblankpage{
  \ifx\@blankpagecontent
    \empty
  \else
    \begin{center}
      \@blankpagecontent
    \end{center}
  \fi
  \thispagestyle{empty}
  \clearpage
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand\csseabstractname{%
  \CYRR\CYRE\CYRF\CYRE\CYRR\CYRA\CYRT
}

\newcommand\taskname{%
  \CYRZ\CYRA\CYRD\CYRA\CYRN\CYRN\CYRI\CYRE\ \CYRN\CYRA\ \CYRV\CYRK\CYRR\ \CYRS\CYRT\CYRU\CYRD\CYRE\CYRN\CYRT\CYRU
}
\newcommand\csseabstractnameen{%
  THE ABSTRACT%
}
\newcommand\stats@report{%
  \CYRN\cyra%
}
\newcommand\stats@report@en{%
}
\newcommand\stats@page{%
  \cyrs.%
}
\newcommand\stats@page@en{%
  pages%
}
\newcommand\stats@fig{%
  \cyrr\cyri\cyrs%
}
\newcommand\stats@fig@en{%
  pictures%
}
\newcommand\stats@table{%
  \cyrt\cyra\cyrb\cyrl%
}
\newcommand\stats@table@en{%
  tables%
}
\newcommand\stats@cite{%
  \cyri\cyrs\cyrt.%
}
\newcommand\stats@cite@en{%
  references%
}
\newcommand\stats@appendix{%
  \cyrp\cyrr\cyri\cyrl\cyro\cyrzh\cyre\cyrn\cyri\cyrishrt%
}
\newcommand\stats@appendix@en{%
  appendicies%
}
\newcommand\@stats[4]{%
  \stats@report%
  \IfEq{#1}{0}{}{%
  ~#1~\stats@page%
  }%
  \IfEq{#2}{0}{}{%
  , #2~\stats@fig%
  }%
  \IfEq{#3}{0}{}{%
  , #3~\stats@table%
  }%
%  \IfEq{#4}{0}{}{%
%  , #4~\stats@cite%
%  }%
  \IfEq{#4}{0}{}{%
  , #4~\stats@appendix%
  }%
}
\newcommand\@stats@en[4]{%
  \stats@report@en%
  \IfEq{#1}{0}{}{%
  #1~\stats@page@en%
  }%
  \IfEq{#2}{0}{}{%
  , #2~\stats@fig@en%
  }%
  \IfEq{#3}{0}{}{%
  , #3~\stats@table@en%
  }%
%  \IfEq{#4}{0}{}{%
%  , #4~\stats@cite@en%
%  }%
  \IfEq{#4}{0}{}{%
  , #4~\stats@appendix@en%
  }%
}

\newcommand\pagecount[1]{%
  \xdef\@pagecount{#1}
}\pagecount{0}
\newcommand\figcount[1]{%
  \xdef\@figcount{#1}
}\figcount{0}
\newcommand\tablecount[1]{%
  \xdef\@tablecount{#1}
}\tablecount{0}
\newcommand\citecount[1]{%
  \xdef\@citecount{#1}
}\citecount{0}
\newcommand\appendixcount[1]{%
  \xdef\@appendixcount{#1}
}\appendixcount{0}

\newcommand\abstractcontent[1]{%
  \gdef\@abstractcontent{#1}
}\abstractcontent{}
\newcommand\abstractcontenten[1]{%
  \gdef\@abstractcontent@en{#1}
}\abstractcontenten{}

\newcommand\keywordsen[1]{%
  \gdef\@keywords@en{#1}
}\keywordsen{}


\newcommand\maketask{%
  %\clearpage%
  \thispagestyle{empty}%
%  %
  \@makeschapterhead{\taskname}%
  \@afterheading%
%  %
  \begin{center}%
	\minobrname\\
        \@institutionlabel\\
	<<\@institution>>\\[0.3cm]
	\@faculty\\[0.3cm]
	\@department
  \end{center}%
  %
  \begin{flushleft}%
      \CYRU\CYRT\CYRV\CYRE\CYRR\CYRZH\CYRD\CYRA\CYRYU:\\
      \appos,\\[-0.2cm]
      \@apstatus\\[-0.2cm]
      \makebox[5cm][r]{\hrulefill}\@apname
  \end{flushleft}
  \begin{center}
     \textbf{
        \CYRZ\CYRA\CYRD\CYRA\CYRN\CYRI\CYRE
     }\\
     \textbf{\cyrn\cyra\ \cyrv\cyrery\cyrp\cyro\cyrl\cyrn\cyre\cyrn\cyri\cyre\ \CYRV\CYRK\CYRR\\
	  \@author\\[-0.5cm]
	  \makebox[\textwidth][c]{\hrulefill}\\[-0.3cm]
	  \textit{\footnotesize(\CYRF\cyra\cyrm\cyri\cyrl\cyri\cyrya, \cyri\cyrm\cyrya, \cyro\cyrt\cyrch\cyre\cyrs\cyrt\cyrv\cyro)}}
  \end{center}
  \begin{flushleft}
    \begin{enumerate}
      \itemsep-0.3cm
      \item \CYRT\cyre\cyrm\cyra\ \CYRV\CYRK\CYRR: \@topic
      \item \CYRS\cyrr\cyro\cyrk\ \cyrs\cyrd\cyra\cyrch\cyri\ \cyrz\cyra\cyrk\cyro\cyrn\cyrch\cyre\cyrn\cyrn\cyro\cyrishrt\ \cyrr\cyra\cyrb\cyro\cyrt\cyrery: \@enddate
      \item \CYRI\cyrs\cyrh\cyro\cyrd\cyrn\cyrery\cyre\ \cyrd\cyra\cyrn\cyrn\cyrery\cyre\ \cyrk\ \cyrr\cyra\cyrb\cyro\cyrt\cyre: \@sourcedata
      \item \CYRS\cyro\cyrd\cyre\cyrzh\cyra\cyrn\cyri\cyre\ \cyrr\cyra\cyrb\cyro\cyrt\cyrery:% \tableofcontents
      \item \CYRP\cyre\cyrr\cyre\cyrch\cyre\cyrn\cyrsftsn\ \cyrg\cyrr\cyra\cyrf\cyri\cyrch\cyre\cyrs\cyrk\cyro\cyrg\cyro \cyri\ \cyri\cyrl\cyrl\cyryu\cyrs\cyrt\cyrr\cyra\cyrt\cyri\cyrv\cyrn\cyro\cyrg\cyro\ (\cyrr\cyra\cyrz\cyrd\cyra\cyrt\cyro\cyrch\cyrn\cyro\cyrg\cyro)\ \cyrm\cyra\cyrt\cyre\cyrr\cyri\cyra\cyrl\cyra: \@graphicdata
      \item \conname\ \CYRV\CYRK\CYRR: \@constatus~\@con
      \item \CYRD\cyra\cyrt\cyra\ \cyrv\cyrery\cyrd\cyra\cyrch\cyri\ \cyrz\cyra\cyrd\cyra\cyrn\cyri\cyrya: \@begindate
    \end{enumerate}
    \begin{tabular*}{\linewidth}{@{\extracolsep{\fill}}lr@{}}
	    \sasndname & \textit{\textbf{\@sa~(\CYRI.~\CYRO.~\CYRF\cyra\cyrm\cyri\cyrl\cyri\cyrya)}}
    \end{tabular*}
    \\[2cm]
    \begin{tabular*}{\linewidth}{@{\extracolsep{\fill}}lcr@{}}
	    \CYRZ\cyra\cyrd\cyra\cyrn\cyri\cyre\ \cyrp\cyrr\cyri\cyrn\cyrya\cyrl\ \cyrk\ \cyri\cyrs\cyrp\cyro\cyrl\cyrn\cyre\cyrn\cyri\cyryu & \hrulefill & \textbf{\textit{(\CYRP\cyro\cyrd\cyrp\cyri\cyrs\cyrsftsn\ \cyrs\cyrt\cyru\cyrd\cyre\cyrn\cyrt\cyra\ \cyri\ \cyrd\cyra\cyrt\cyra)}}
    \end{tabular*}
  \end{flushleft}
  %
%   {%
%     \hskip\parindent%
%   }
}

% \newcommand\maketask{%
%   \@maketask{%
%     \taskname
%     }{%
%     }
%     {}{%
%     \@abstractcontent%
%   }
% }

\newcommand\@makeabstract[4]{%
  %\clearpage%
  %\thispagestyle{empty}%
  %
  \@makeschapterhead{#1}%
  \@afterheading%
  %
  \begin{flushleft}%
  ~~~~~ #2%
  \end{flushleft}%
  %
  \begin{flushleft}%
    \MakeUppercase{#3}%
  \end{flushleft}
  %
  {%
    \hskip\parindent%
    #4%
  }
}

\newcommand\makeabstractru{%
  \@makeabstract{%
    \csseabstractnameen}{%
    \@stats{%
      \@pagecount}{%
      \@figcount}{%
      \@tablecount}{%
      \@appendixcount%
    }}{%
    \@keywords}{%
    \@abstractcontent%
  }
}
\newcommand\makeabstracten{%
  \@makeabstract{%
    \csseabstractnameen}{%
    \@stats@en{%
      \@pagecount}{%
      \@figcount}{%
      \@tablecount}{%
      \@appendixcount%
    }}{%
    \@keywords@en}{%
    \@abstractcontent@en%
  }
}
\newcommand\makeabstract{%
  \makeabstractru%

  \makeabstracten%

  \clearpage%
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcounter{total@page}
\newcounter{total@figure}
\newcounter{total@table}
\newcounter{total@cite}
\newcounter{total@appendix}

\gdef\total@page{0}
\gdef\total@figure{0}
\gdef\total@table{0}
\gdef\total@cite{0}
\gdef\total@appendix{0}

\AtEndOfClass{
\AtBeginDocument{
  \pagecount{\total@page}
  \figcount{\total@figure}
  \tablecount{\total@table}
  \citecount{\total@cite}
  \appendixcount{\total@appendix}
}
}

\AtEndDocument{
  \setcounter{total@page}{\value{page}}
  %
  \addtocounter{total@figure}{\value{figure}}
  \addtocounter{total@table}{\value{table}}
  %
  \immediate\write\@mainaux{
    \string\gdef\string\total@page{\number\value{total@page}}
    \string\gdef\string\total@figure{\number\value{total@figure}}
    \string\gdef\string\total@table{\number\value{total@table}}
    \string\gdef\string\total@cite{\number\value{total@cite}}
    \string\gdef\string\total@appendix{\number\value{total@appendix}}
  }
}

\def\oldchapter{}
\let\oldchapter=\chapter
\def\chapter{%
  \addtocounter{total@figure}{\value{figure}}%
  \addtocounter{total@table}{\value{table}}%
  \stepcounter{total@appendix}%
  \oldchapter%
}

\def\old@chapter[#1]#2{}
\let\old@chapter=\@chapter
\def\@chapter[#1]#2{
  \old@chapter[\texorpdfstring{\noexpand\MakeTextUppercase{#1}}{#1}]{\MakeTextUppercase{#2}}
}

\def\oldbibcite{}
\let\oldbibcite=\bibcite
\def\bibcite{%
  \stepcounter{total@cite}%
  \oldbibcite%
}

\def\oldappendix{}
\let\oldappendix=\appendix
\def\appendix{%
  \setcounter{total@appendix}{0}%
  \oldappendix%
}

\def\oldnoappendix{}
\let\oldnoappendix=\noappendix
\def\noappendix{%
  \setcounter{total@appendix}{0}%
  \oldnoappendix%
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Default settings

\AtEndOfClass{
\AtBeginDocument{

\clearpage

\institutionlabel{{
	\cyr\CYRF\cyre\cyrd\cyre\cyrr\cyra\cyrl\cyrsftsn\cyrn\cyro\cyre\ \cyrg\cyro\cyrs\cyru\cyrd\cyra\cyrr\cyrs\cyrt\cyrv\cyre\cyrn\cyrn\cyro\cyre\ \cyrb\cyryu\cyrd\cyrzh\cyre\cyrt\cyrn\cyro\cyre\ \cyro\cyrb\cyrr\cyra\cyrz\cyro\cyrv\cyra\cyrt\cyre\cyrl\cyrsftsn\cyrn\cyro\cyre\ \cyru\cyrch\cyrr\cyre\cyrzh\cyrd\cyre\cyrn\cyri\cyre\ \cyrv\cyrery\cyrs\cyrsh\cyre\cyrg\cyro\ \cyro\cyrb\cyrr\cyra\cyrz\cyro\cyrv\cyra\cyrn\cyri\cyrya\
}}
\institution{{\cyr\CYRS\CYRA\CYRN\CYRK\CYRT-\CYRP\CYRE\CYRT\CYRE\CYRR\CYRB\CYRU\CYRR\CYRG\CYRS\CYRK\CYRI\CYRISHRT\ \CYRG\CYRO\CYRS\CYRU\CYRD\CYRA\CYRR\CYRS\CYRT\CYRV\CYRE\CYRN\CYRN\CYRERY\CYRISHRT\ \char221\CYRK\CYRO\CYRN\CYRO\CYRM\CYRI\CYRCH\CYRE\CYRS\CYRK\CYRI\CYRISHRT\ \CYRU\CYRN\CYRI\CYRV\CYRE\CYRR\CYRS\CYRI\CYRT\CYRE\CYRT
}}
\faculty{{\cyr\CYRG\cyru\cyrm\cyra\cyrn\cyri\cyrt\cyra\cyrr\cyrn\cyrery\cyrishrt\ \cyrf\cyra\cyrk\cyru\cyrl\cyrsftsn\cyrt\cyre\cyrt\ %
}}
\department{{\cyr\CYRK\cyra\cyrf\cyre\cyrd\cyrr\cyra\ \cyrr\cyre\cyrg\cyri\cyro\cyrn\cyra\cyrl\cyrsftsn\cyrn\cyro\cyrishrt\ \char253\cyrk\cyro\cyrn\cyro\cyrm\cyri\cyrk\cyri \cyri\ \cyrp\cyrr\cyri\cyrr\cyro\cyrd\cyro\cyrp\cyro\cyrl\cyrsftsn\cyrz\cyro\cyrv\cyra\cyrn\cyri\cyrya}}

\city{{\cyr\CYRS\cyra\cyrn\cyrk\cyrt-\CYRP\cyre\cyrt\cyre\cyrr\cyrb\cyru\cyrr\cyrg}}
\date{\number\year}

%\renewcommand{\courselabel}{}
%\renewcommand{\masterproglabel}{}

\addto\captionsrussian{%
  \renewcommand{\nomname}{\CYRS\CYRP\CYRI\CYRS\CYRO\CYRK\ \CYRO\CYRB\CYRO\CYRZ\CYRN\CYRA\CYRCH\CYRE\CYRN\CYRI\CYRISHRT\ \CYRI\ \CYRS\CYRO\CYRK\CYRR\CYRA\CYRSHCH\CYRE\CYRN\CYRI\CYRISHRT}
  \renewcommand{\contentsname}{\CYRS\CYRO\CYRD\CYRE\CYRR\CYRZH\CYRA\CYRN\CYRI\CYRE}
  \renewcommand{\introname}{\CYRV\CYRV\CYRE\CYRD\CYRE\CYRN\CYRI\CYRE}
  \renewcommand{\conclusionname}{\CYRZ\CYRA\CYRK\CYRL\CYRYU\CYRCH\CYRE\CYRN\CYRI\CYRE}
  \renewcommand{\bibname}{\CYRS\CYRP\CYRI\CYRS\CYRO\CYRK\ \CYRI\CYRS\CYRP\CYRO\CYRL\CYRSFTSN\CYRZ\CYRO\CYRV\CYRA\CYRN\CYRN\CYRERY\CYRH\ \CYRI\CYRS\CYRT\CYRO\CYRCH\CYRN\CYRI\CYRK\CYRO\CYRV}
  \renewcommand{\appendixname}{\CYRP\CYRR\CYRI\CYRL\CYRO\CYRZH\CYRE\CYRN\CYRI\CYRE}
  \renewcommand{\figurename}{\CYRR\cyri\cyrs.}
  \renewcommand{\tablename}{\CYRT\cyra\cyrb\cyrl\cyri\cyrc\cyra}
}

\setcounter{tocdepth}{2}
\setcounter{secnumdepth}{2}

\renewcommand{\tocprethechapter}{}
\renewcommand{\tocpostthechapter}{~}
\renewcommand{\prethechapter}{}
\renewcommand{\postthechapter}{~}
\renewcommand{\tocprethesection}{}
\renewcommand{\tocpostthesection}{~}
\renewcommand{\prethesection}{}
\renewcommand{\postthesection}{~}
\renewcommand{\tocprethesubsection}{}
\renewcommand{\tocpostthesubsection}{~}
\renewcommand{\prethesubsection}{}
\renewcommand{\postthesubsection}{~}
\renewcommand{\tocprethesubsubsection}{}
\renewcommand{\tocpostthesubsubsection}{~}
\renewcommand{\prethesubsubsection}{}
\renewcommand{\postthesubsubsection}{~}
\renewcommand{\tocpretheappendix}{\appendixname~}
\renewcommand{\tocposttheappendix}{.~}
\renewcommand{\pretheappendix}{\appendixname~}
\renewcommand{\posttheappendix}{\\}

\if@usedotsinheaders
  \renewcommand{\tocpostthechapter}{.~}
  \renewcommand{\postthechapter}{.~}
  \renewcommand{\tocpostthesection}{.~}
  \renewcommand{\postthesection}{.~}
  \renewcommand{\tocpostthesubsection}{.~}
  \renewcommand{\postthesubsection}{.~}
  \renewcommand{\tocpostthesubsubsection}{.~}
  \renewcommand{\postthesubsubsection}{.~}
\fi

\renewcommand\floatpagefraction{0.9}
\renewcommand\topfraction{0.9}
\renewcommand\bottomfraction{0.9}
\renewcommand\textfraction{0.1}
\setcounter{totalnumber}{64}
\setcounter{topnumber}{64}
\setcounter{bottomnumber}{64}

\DeclareCaptionFormat{hfillstart}{\hfill#1#2#3\par}
\captionsetup{singlelinecheck=false}
\captionsetup[figure]{%
    font=normal,
  labelsep=period,
  justification=centering}
% \captionsetup[table]{%
%   labelsep=period,
%   justification=raggedleft}
\captionsetup[table]{ 
    font=normal,
    format=hfillstart,
    labelsep=newline,
    justification=centering,
    skip=-10pt}
% \floatname{figure}{\CYRR\cyri\cyrs\cyru\cyrn\cyro\cyrk}
\floatname{figure}{\CYRR\cyri\cyrs}
\floatname{table}{\CYRT\cyra\cyrb\cyrl\cyri\cyrc\cyra}

% Workaround for disser-1.2.0 `headcenter` pagestyle options
% Custom page numbering style

\usepackage{fancyhdr}
\setlength{\headheight}{15pt}

\pagestyle{fancy}
\chapterpagestyle{fancy}
\renewcommand{\chaptermark}[1]{ \markboth{#1}{} }
\renewcommand{\sectionmark}[1]{ \markboth{#1}{} }

\fancyhf{}
\fancyhead[LE,RO]{\thepage}
% \fancyfoot[LE,RO]{\thepage}
% \fancyhead[R]{\thepage}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\fancypagestyle{plain}{
  \fancyhf{}
  \fancyhead[LE,RO]{\thepage}
%  \fancyfoot[LE,RO]{\thepage}
%  \fancyhead[R]{\thepage}
  \renewcommand{\headrulewidth}{0pt}
  \renewcommand{\footrulewidth}{0pt}
}

% Workaround for new babel-russian conventions for alphabetic counters
\newcommand{\@Asbuk}{\russian@Alph}
\newcommand{\@asbuk}{\russian@alph}

\def\logo#1{\gdef\@logo{#1}}\logo{}

}
}
